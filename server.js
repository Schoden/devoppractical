const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path: './config.env'})
const app = require('./app') // require helps in iomport(using path)
const port = 5000
// const DB = process.env.DATABASE.replace(
//         '<PASSWORD>',
//         process.env.DATABASE_PASSWORD,
// )
   
mongoose.connect(process.env.DATABASE_LOCAL).then((con) => {
    // console.log(con.connections)
    console.log('DB connection successful')
}).catch(error => console.log(error))

//arrow function
// app.listen(port, () =>{
//     console.log(`app running on port:${port}..`)
// =============normal function================
app.listen (port, function(){
        console.log(`app running on port:${port}..`)
})
