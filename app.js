const express = require("express") // do not want to change

const app = express() // call express
app.use(express.json())
const userRoutes= require('./routes/userRoute')
app.use('/api/v1/users',userRoutes)


module.exports = app